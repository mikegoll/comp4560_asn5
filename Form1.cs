using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.IO;

namespace asgn5v1
{
	/// <summary>
	/// Summary description for Transformer.
	/// </summary>
	public class Transformer : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		//private bool GetNewData();

		// basic data for Transformer

		int numpts = 0;
		int numlines = 0;
		bool gooddata = false;		
		double[,] vertices;
		double[,] scrnpts;
		double[,] ctrans = new double[4,4];  //your main transformation matrix
		private System.Windows.Forms.ImageList tbimages;
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.ToolBarButton transleftbtn;
		private System.Windows.Forms.ToolBarButton transrightbtn;
		private System.Windows.Forms.ToolBarButton transupbtn;
		private System.Windows.Forms.ToolBarButton transdownbtn;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton scaleupbtn;
		private System.Windows.Forms.ToolBarButton scaledownbtn;
		private System.Windows.Forms.ToolBarButton toolBarButton2;
		private System.Windows.Forms.ToolBarButton rotxby1btn;
		private System.Windows.Forms.ToolBarButton rotyby1btn;
		private System.Windows.Forms.ToolBarButton rotzby1btn;
		private System.Windows.Forms.ToolBarButton toolBarButton3;
		private System.Windows.Forms.ToolBarButton rotxbtn;
		private System.Windows.Forms.ToolBarButton rotybtn;
		private System.Windows.Forms.ToolBarButton rotzbtn;
		private System.Windows.Forms.ToolBarButton toolBarButton4;
		private System.Windows.Forms.ToolBarButton shearrightbtn;
		private System.Windows.Forms.ToolBarButton shearleftbtn;
		private System.Windows.Forms.ToolBarButton toolBarButton5;
		private System.Windows.Forms.ToolBarButton resetbtn;
		private System.Windows.Forms.ToolBarButton exitbtn;
		int[,] lines;


        double maxX, maxY, maxZ;

        //for continuous rotations
        Timer timer = new Timer();
        bool running = false;
        int timerSelection = 0;

		public Transformer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            this.ResizeEnd += new EventHandler(Form1_ResizeEnd);

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.DoubleBuffer, true);
			Text = "COMP 4560:  Assignment 5 (200830) (Michael Goll)";
			ResizeRedraw = true;
			BackColor = Color.Black;
			MenuItem miNewDat = new MenuItem("New &Data...",
				new EventHandler(MenuNewDataOnClick));
			MenuItem miExit = new MenuItem("E&xit", 
				new EventHandler(MenuFileExitOnClick));
			MenuItem miDash = new MenuItem("-");
			MenuItem miFile = new MenuItem("&File",
				new MenuItem[] {miNewDat, miDash, miExit});
			MenuItem miAbout = new MenuItem("&About",
				new EventHandler(MenuAboutOnClick));
			Menu = new MainMenu(new MenuItem[] {miFile, miAbout});
            timer.Tick += timerTick;
            timer.Interval = 10;
		}

        private void timerTick(object sender, EventArgs e)
        {
            Rotate(timerSelection, 0.05);
            Refresh();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Transformer));
            this.tbimages = new System.Windows.Forms.ImageList(this.components);
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.transleftbtn = new System.Windows.Forms.ToolBarButton();
            this.transrightbtn = new System.Windows.Forms.ToolBarButton();
            this.transupbtn = new System.Windows.Forms.ToolBarButton();
            this.transdownbtn = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.scaleupbtn = new System.Windows.Forms.ToolBarButton();
            this.scaledownbtn = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.rotxby1btn = new System.Windows.Forms.ToolBarButton();
            this.rotyby1btn = new System.Windows.Forms.ToolBarButton();
            this.rotzby1btn = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.rotxbtn = new System.Windows.Forms.ToolBarButton();
            this.rotybtn = new System.Windows.Forms.ToolBarButton();
            this.rotzbtn = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.shearrightbtn = new System.Windows.Forms.ToolBarButton();
            this.shearleftbtn = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.resetbtn = new System.Windows.Forms.ToolBarButton();
            this.exitbtn = new System.Windows.Forms.ToolBarButton();
            this.SuspendLayout();
            // 
            // tbimages
            // 
            this.tbimages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("tbimages.ImageStream")));
            this.tbimages.TransparentColor = System.Drawing.Color.Transparent;
            this.tbimages.Images.SetKeyName(0, "");
            this.tbimages.Images.SetKeyName(1, "");
            this.tbimages.Images.SetKeyName(2, "");
            this.tbimages.Images.SetKeyName(3, "");
            this.tbimages.Images.SetKeyName(4, "");
            this.tbimages.Images.SetKeyName(5, "");
            this.tbimages.Images.SetKeyName(6, "");
            this.tbimages.Images.SetKeyName(7, "");
            this.tbimages.Images.SetKeyName(8, "");
            this.tbimages.Images.SetKeyName(9, "");
            this.tbimages.Images.SetKeyName(10, "");
            this.tbimages.Images.SetKeyName(11, "");
            this.tbimages.Images.SetKeyName(12, "");
            this.tbimages.Images.SetKeyName(13, "");
            this.tbimages.Images.SetKeyName(14, "");
            this.tbimages.Images.SetKeyName(15, "");
            // 
            // toolBar1
            // 
            this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.transleftbtn,
            this.transrightbtn,
            this.transupbtn,
            this.transdownbtn,
            this.toolBarButton1,
            this.scaleupbtn,
            this.scaledownbtn,
            this.toolBarButton2,
            this.rotxby1btn,
            this.rotyby1btn,
            this.rotzby1btn,
            this.toolBarButton3,
            this.rotxbtn,
            this.rotybtn,
            this.rotzbtn,
            this.toolBarButton4,
            this.shearrightbtn,
            this.shearleftbtn,
            this.toolBarButton5,
            this.resetbtn,
            this.exitbtn});
            this.toolBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolBar1.DropDownArrows = true;
            this.toolBar1.ImageList = this.tbimages;
            this.toolBar1.Location = new System.Drawing.Point(484, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ShowToolTips = true;
            this.toolBar1.Size = new System.Drawing.Size(24, 306);
            this.toolBar1.TabIndex = 0;
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // transleftbtn
            // 
            this.transleftbtn.ImageIndex = 1;
            this.transleftbtn.Name = "transleftbtn";
            this.transleftbtn.ToolTipText = "translate left";
            // 
            // transrightbtn
            // 
            this.transrightbtn.ImageIndex = 0;
            this.transrightbtn.Name = "transrightbtn";
            this.transrightbtn.ToolTipText = "translate right";
            // 
            // transupbtn
            // 
            this.transupbtn.ImageIndex = 2;
            this.transupbtn.Name = "transupbtn";
            this.transupbtn.ToolTipText = "translate up";
            // 
            // transdownbtn
            // 
            this.transdownbtn.ImageIndex = 3;
            this.transdownbtn.Name = "transdownbtn";
            this.transdownbtn.ToolTipText = "translate down";
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.Name = "toolBarButton1";
            this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // scaleupbtn
            // 
            this.scaleupbtn.ImageIndex = 4;
            this.scaleupbtn.Name = "scaleupbtn";
            this.scaleupbtn.ToolTipText = "scale up";
            // 
            // scaledownbtn
            // 
            this.scaledownbtn.ImageIndex = 5;
            this.scaledownbtn.Name = "scaledownbtn";
            this.scaledownbtn.ToolTipText = "scale down";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.Name = "toolBarButton2";
            this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // rotxby1btn
            // 
            this.rotxby1btn.ImageIndex = 6;
            this.rotxby1btn.Name = "rotxby1btn";
            this.rotxby1btn.ToolTipText = "rotate about x by 1";
            // 
            // rotyby1btn
            // 
            this.rotyby1btn.ImageIndex = 7;
            this.rotyby1btn.Name = "rotyby1btn";
            this.rotyby1btn.ToolTipText = "rotate about y by 1";
            // 
            // rotzby1btn
            // 
            this.rotzby1btn.ImageIndex = 8;
            this.rotzby1btn.Name = "rotzby1btn";
            this.rotzby1btn.ToolTipText = "rotate about z by 1";
            // 
            // toolBarButton3
            // 
            this.toolBarButton3.Name = "toolBarButton3";
            this.toolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // rotxbtn
            // 
            this.rotxbtn.ImageIndex = 9;
            this.rotxbtn.Name = "rotxbtn";
            this.rotxbtn.ToolTipText = "rotate about x continuously";
            // 
            // rotybtn
            // 
            this.rotybtn.ImageIndex = 10;
            this.rotybtn.Name = "rotybtn";
            this.rotybtn.ToolTipText = "rotate about y continuously";
            // 
            // rotzbtn
            // 
            this.rotzbtn.ImageIndex = 11;
            this.rotzbtn.Name = "rotzbtn";
            this.rotzbtn.ToolTipText = "rotate about z continuously";
            // 
            // toolBarButton4
            // 
            this.toolBarButton4.Name = "toolBarButton4";
            this.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // shearrightbtn
            // 
            this.shearrightbtn.ImageIndex = 12;
            this.shearrightbtn.Name = "shearrightbtn";
            this.shearrightbtn.ToolTipText = "shear right";
            // 
            // shearleftbtn
            // 
            this.shearleftbtn.ImageIndex = 13;
            this.shearleftbtn.Name = "shearleftbtn";
            this.shearleftbtn.ToolTipText = "shear left";
            // 
            // toolBarButton5
            // 
            this.toolBarButton5.Name = "toolBarButton5";
            this.toolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // resetbtn
            // 
            this.resetbtn.ImageIndex = 14;
            this.resetbtn.Name = "resetbtn";
            this.resetbtn.ToolTipText = "restore the initial image";
            // 
            // exitbtn
            // 
            this.exitbtn.ImageIndex = 15;
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.ToolTipText = "exit the program";
            // 
            // Transformer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(508, 306);
            this.Controls.Add(this.toolBar1);
            this.Name = "Transformer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Transformer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
		}
		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Transformer());
		}

		protected override void OnPaint(PaintEventArgs pea)
		{
			Graphics grfx = pea.Graphics;
         Pen pen = new Pen(Color.White, 3);
			double temp;
			int k;

            if (gooddata)
            {
                //create the screen coordinates:
                //scrnpts = vertices * ctrans

                for (int i = 0; i < numpts; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        temp = 0.0d;
                        for (k = 0; k < 4; k++)
                            temp += vertices[i, k] * ctrans[k, j];
                        scrnpts[i, j] = temp;
                    }
                }

                //now draw the lines

                for (int i = 0; i < numlines; i++)
                {
                    grfx.DrawLine(pen, (int)scrnpts[lines[i, 0], 0], (int)scrnpts[lines[i, 0], 1],
                        (int)scrnpts[lines[i, 1], 0], (int)scrnpts[lines[i, 1], 1]);
                }


            } // end of gooddata block	
		} // end of OnPaint

		void MenuNewDataOnClick(object obj, EventArgs ea)
		{
			//MessageBox.Show("New Data item clicked.");
			gooddata = GetNewData();
            if (gooddata)
                center(ctrans);
			RestoreInitialImage();			
		}

		void MenuFileExitOnClick(object obj, EventArgs ea)
		{
			Close();
		}

		void MenuAboutOnClick(object obj, EventArgs ea)
		{
			AboutDialogBox dlg = new AboutDialogBox();
			dlg.ShowDialog();
		}

		void RestoreInitialImage()
		{
			Invalidate();
		} // end of RestoreInitialImage

		bool GetNewData()
		{
			string strinputfile,text;
			ArrayList coorddata = new ArrayList();
			ArrayList linesdata = new ArrayList();
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.Title = "Choose File with Coordinates of Vertices";
			if (opendlg.ShowDialog() == DialogResult.OK)
			{
				strinputfile=opendlg.FileName;				
				FileInfo coordfile = new FileInfo(strinputfile);
				StreamReader reader = coordfile.OpenText();
				do
				{
					text = reader.ReadLine();
					if (text != null) coorddata.Add(text);
				} while (text != null);
				reader.Close();
				DecodeCoords(coorddata);
			}
			else
			{
				MessageBox.Show("***Failed to Open Coordinates File***");
				return false;
			}
            
			opendlg.Title = "Choose File with Data Specifying Lines";
			if (opendlg.ShowDialog() == DialogResult.OK)
			{
				strinputfile=opendlg.FileName;
				FileInfo linesfile = new FileInfo(strinputfile);
				StreamReader reader = linesfile.OpenText();
				do
				{
					text = reader.ReadLine();
					if (text != null) linesdata.Add(text);
				} while (text != null);
				reader.Close();
				DecodeLines(linesdata);
			}
			else
			{
				MessageBox.Show("***Failed to Open Line Data File***");
				return false;
			}
			scrnpts = new double[numpts,4];
			setIdentity(ctrans,4,4);  //initialize transformation matrix to identity
			return true;
		} // end of GetNewData

		void DecodeCoords(ArrayList coorddata)
		{
			//this may allocate slightly more rows that necessary
			vertices = new double[coorddata.Count,4];
			numpts = 0;
			string [] text = null;
			for (int i = 0; i < coorddata.Count; i++)
			{
				text = coorddata[i].ToString().Split(' ',',');
				vertices[numpts,0]=double.Parse(text[0]);
				if (vertices[numpts,0] < 0.0d) break;
				vertices[numpts,1]=double.Parse(text[1]);
				vertices[numpts,2]=double.Parse(text[2]);
				vertices[numpts,3] = 1.0d;
				numpts++;						
			}
			
		}// end of DecodeCoords

		void DecodeLines(ArrayList linesdata)
		{
			//this may allocate slightly more rows that necessary
			lines = new int[linesdata.Count,2];
			numlines = 0;
			string [] text = null;
			for (int i = 0; i < linesdata.Count; i++)
			{
				text = linesdata[i].ToString().Split(' ',',');
				lines[numlines,0]=int.Parse(text[0]);
				if (lines[numlines,0] < 0) break;
				lines[numlines,1]=int.Parse(text[1]);
				numlines++;						
			}
		} // end of DecodeLines

		void setIdentity(double[,] A,int nrow,int ncol)
		{
			for (int i = 0; i < nrow;i++) 
			{
				for (int j = 0; j < ncol; j++) A[i,j] = 0.0d;
				A[i,i] = 1.0d;
			}
		}// end of setIdentity
      

		private void Transformer_Load(object sender, System.EventArgs e)
		{
			
		}

		private void toolBar1_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{

            int transX = 10, transY = 10;
            double rotationFactor = 0.05, scaleFactor = 0.10, shearFactorX = 0.10;

			if (e.Button == transleftbtn)
			{
                translateX(-transX);
				Refresh();
			}
			if (e.Button == transrightbtn) 
			{
                translateX(transX);
				Refresh();
			}
			if (e.Button == transupbtn)
			{
                translateY(-transY);
				Refresh();
			}
			
			if(e.Button == transdownbtn)
			{
                translateY(transY);
				Refresh();
			}
			if (e.Button == scaleupbtn) 
			{
                Scale(scaleFactor);
				Refresh();
			}
			if (e.Button == scaledownbtn) 
			{
                Scale(-scaleFactor);
                Refresh();
			}
			if (e.Button == rotxby1btn) 
			{
                Rotate(0, rotationFactor);
                Refresh();
            }
			if (e.Button == rotyby1btn) 
			{
                Rotate(1, rotationFactor);
                Refresh();
            }
			if (e.Button == rotzby1btn) 
			{
                Rotate(2, rotationFactor);
                Refresh();
            }

			if (e.Button == rotxbtn) 
			{
                if (!running)
                {
                    running = true;
                    timerSelection = 0;
                    timer.Start();
                }
                else
                {
                    running = false;
                    timer.Stop();
                }
			}

			if (e.Button == rotybtn) 
			{
                if (!running)
                {
                    running = true;
                    timerSelection = 1;
                    timer.Start();
                }
                else
                {
                    running = false;
                    timer.Stop();
                }
            }
			
			if (e.Button == rotzbtn) 
			{
                if (!running)
                {
                    running = true;
                    timerSelection = 2;
                    timer.Start();
                }
                else
                {
                    running = false;
                    timer.Stop();
                }
            }

			if (e.Button == shearleftbtn)
			{
                Shear(shearFactorX);
				Refresh();
			}

			if (e.Button == shearrightbtn) 
			{
                Shear(-shearFactorX);
				Refresh();
			}

			if (e.Button == resetbtn)
			{
                resetImage();
			}

			if(e.Button == exitbtn) 
			{
				Close();
			}

		}

        //---------------------------- My Stuff ----------------------------

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            resetImage();
        }

        /** Resets the shape to the center of the screen */
        private void resetImage()
        {
            setIdentity(ctrans, 4, 4);
            center(ctrans);
            RestoreInitialImage();
        }

        /** Multiplies two matrices together */
        void multiplyMatrix(double[,] netTrans)
        {
            double[,] temp = new double[4, 4];

            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    for (int k = 0; k < 4; ++k)
                        temp[i, j] += ctrans[i, k] * netTrans[k, j];

            ctrans = temp;
        }

        /** Centers the shape */
        private void center(double[,] ctrans)
        {
            maxX = vertices[0, 0];
            maxY = vertices[0, 1];
            maxZ = vertices[0, 2];

            for (int i = 0; i < numpts; ++i)
            {
                if (maxX < vertices[i, 0])
                    maxX = vertices[i, 0];
                if (maxY < vertices[i, 1])
                    maxY = vertices[i, 1];
                if (maxZ < vertices[i, 2])
                    maxZ = vertices[i, 2];
            }

            double maxHeight = ctrans[0, 0];

            //finds max height in vertices
            for (int i = 0; i < (vertices.Length / 4); i++)
            {
                if (vertices[i, 1] > maxHeight)
                    maxHeight = vertices[i, 1];
            }
            
            //get the scale relative to the window size
            double scale = (((double)this.ClientRectangle.Height) / 2) / maxHeight;

            //Tnet matrix for the centering and scaling of the shape
            double[,] matrix = {
                                    {scale, 0, 0, 0},
                                    {0, scale * -1, 0, 0},
                                    {0, 0, scale, 0},
                                    {
                                        ((double)this.ClientRectangle.Width) / 2 - (vertices[0, 0] * scale),  //translates the shape to the center of the screen width
                                        ((double)this.ClientRectangle.Height) / 2 + (vertices[0, 0] * scale), //translates the shape to the center of the screen height
                                        0, 1
                                    }
                                };

            multiplyMatrix(matrix);
        }

        /** Translates the shape horizontally by given value */
        private void translateX(int value)
        {
            double[,] tNet = {
                                 {1, 0, 0, 0},
                                 {0, 1, 0, 0},
                                 {0, 0, 1, 0},
                                 {value, 0, 0, 1}
                             };

            multiplyMatrix(tNet);
        }

        /** Translates the shape vertically by given value */
        private void translateY(int value)
        {
            double[,] tNet = {
                                 {1, 0, 0, 0},
                                 {0, 1, 0, 0},
                                 {0, 0, 1, 0},
                                 {0, value, 0, 1}
                             };

            multiplyMatrix(tNet);
        }

        /** Translates the shape to the origin point (0, 0) */
        private double[] transToZero()
        {
            //Find center point screen coordinates
            double centerPointX = scrnpts[0, 0];

            double centerPointY = scrnpts[0, 1];

            double centerPointZ = scrnpts[0, 2];


            //translate to origin point (0, 0)
            double[,] tNet = new double[,]
            {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {
                    -centerPointX,

                    -centerPointY,

                    -centerPointZ,
                    1
                }
            };

            multiplyMatrix(tNet);

            double[] values = {centerPointX, centerPointY, centerPointZ};
            return values;
        }

        /** Translates the shape to its original position */
        private void transBack(double[] centerPoints)
        {
            //Return center point back to where it was
            double[,] tNet = new double[,]
            {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {
                    centerPoints[0],

                    centerPoints[1],

                    centerPoints[2],

                    1
                }
            };

            multiplyMatrix(tNet);
        }

        /** Rotates the shape in a certain direction by a certain value in radians */
        private void Rotate(int direction, double radian)
        {

            //translate the shape to origin point
            double[] centerPoints = transToZero();

            double[,] tNet = {
                                {1, 0, 0, 0},
                                {0, 1, 0, 0},
                                {0, 0, 1, 0},
                                {0, 0, 0, 1}
                            };

            // Calculate rotate cos and sin value and put them into the matrix
            double cos = Math.Cos(radian);
            double sin = Math.Sin(radian);

            //interpret which direction rotation is in
            switch (direction)
            {
                case 0:
                    tNet[1, 1] = cos;
                    tNet[1, 2] = sin;
                    tNet[2, 1] = -sin;
                    tNet[2, 2] = cos;
                    break;
                case 1:
                    tNet[0, 0] = cos;
                    tNet[0, 2] = sin;
                    tNet[2, 0] = -sin;
                    tNet[2, 2] = cos;
                    break;
                case 2:
                    tNet[0, 0] = cos;
                    tNet[0, 1] = sin;
                    tNet[1, 0] = -sin;
                    tNet[1, 1] = cos;
                    break;
                default:
                    break;
            }

            multiplyMatrix(tNet);

            //translate the shape back
            transBack(centerPoints);
        }

        /** Scales the shape by a certain factor */
        private void Scale(double factor)
        {
            //translate shape to origin point (0, 0)
            double[] centerPoints = transToZero();

            //apply the scaling
            double[,] tNet = {
                                {1 + factor, 0, 0, 0},
                                {0, 1 + factor, 0, 0},
                                {0, 0, 1 + factor, 0},
                                {0, 0, 0, 1}
                            };

            multiplyMatrix(tNet);

            //translate shape back to original position
            transBack(centerPoints);
        }

        /** Shears the shape by a certain factor */
        private void Shear(double shearFactor)
        {
            double[,] transToZero = new double[4, 4];
            double[,] tNet = new double[4, 4];
            double[,] transback = new double[4, 4];

            setIdentity(transToZero, 4, 4);
            setIdentity(tNet, 4, 4);
            setIdentity(transback, 4, 4);

            //translate to origin point
            transToZero[3, 0] = -scrnpts[0, 0] + ctrans[0, 0] * maxX / 2; //move center to 0,0 and then half the shape over
            transToZero[3, 1] = -scrnpts[0, 1] + ctrans[1, 1] * maxY / 2; //move center to 0,0 and then move up by half the height
            transToZero[3, 2] = -scrnpts[0, 2];
            multiplyMatrix(transToZero);

            //shear the shape
            tNet[1, 0] = shearFactor;
            multiplyMatrix(tNet);


            //translate back to where it was
            transback[3, 0] = scrnpts[0, 0] - ctrans[0, 0] * maxX / 2;
            transback[3, 1] = scrnpts[0, 1] - ctrans[1, 1] * maxY / 2;
            transback[3, 2] = scrnpts[0, 2];

            multiplyMatrix(transback);
        }
    }
}
